package hw;

import java.util.Scanner;

public class Main extends Methods {

    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        boolean exeptionKostil = false; // сорямба =D
        String[] operation;
        String[] valuesAB;
        String inputValue;
        double result;
        while (true) {
            result = 0.0;
            System.out.println("Введите операцию");
            inputValue = sc.nextLine();
            if (inputValue.equals("quit")) {
                System.out.println("работа программы завершена");
                break; // выход из цикла while
            }
            if (isDigits(inputValue)) {
                valuesAB = inputValue.replaceAll(" ", "").replaceAll(",", ".").split("[*]|[/]|[+]|[-]");
                operation = inputValue.replaceAll("[0-9]|[.]", "").replaceAll(" ", "").split("");
                for (int j = 0; j < operation.length; j++) {
                    if (operation[j].equals("/") && valuesAB[j + 1].equals("0"))
                        System.out.println("Делить на 0 нельзя =)");
                }
                result = Double.valueOf(valuesAB[0]); // результату присваиваем первое значение
                mathOperations(result, operation, valuesAB);

            } else {
                System.out.printf("упс, в вашем вражении содержатся посторонние символы, вы ввели: \"%s\" попробуйте еще разок =)\n", inputValue);
                exeptionKostil = true; // костыль, чтобы в конце не выводился невалидный result
            }
            if (String.valueOf(result).equals("Infinity") || exeptionKostil) {
                exeptionKostil = false;
            } else
                System.out.printf("%s = %s\n", inputValue, result);
        }
    }
}
