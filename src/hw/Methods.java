package hw;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Methods {

    static boolean isDigits(String str) {
        String pattern = ("[a-zA-Z]|[а-яА-Я]|[!@#$%^&~?:;<>()\'\"=]");
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(str);
        return !m.find();
    }

    static double mathOperations(double result, String operation[], String valuesAB[]) {
        for (int i = 0; i < operation.length; i++) {
            switch (operation[i]) {
                case "*": {
                    result *= Double.valueOf(valuesAB[i + 1]);
                    break;
                }
                case "/": {
                    result /= Double.valueOf(valuesAB[i + 1]);
                    break;
                }
                case "+": {
                    result += Double.valueOf(valuesAB[i + 1]);
                    break;
                }
                case "-":
                    result -= Double.valueOf(valuesAB[i + 1]);
                    break;
            }
        }
        return result;
    }
}
